import { Component } from '@angular/core';
import {Card} from '../model/card';
import { NavParams,ModalController, NavController } from 'ionic-angular';
import {Subscription} from "rxjs";
import {TimerObservable} from "rxjs/observable/TimerObservable";
import {ModalWinEasyPage } from "../../pages/modal-win-easy/modal-win-easy";
import {ModalMenuPage } from "../../pages/modal-menu/modal-menu";
import {ModalTutoPage } from "../../pages/modal-tuto/modal-tuto";
//import {PblAppServiceProvider} from '../service/pbl-app-service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'flash-card',
  templateUrl: 'flash-card.html'
  //,providers: [/*PblAppServiceProvider*/]
})
export class FlashCardComponent  {

  private subscription: Subscription;
  flipped: boolean = true;
  card:Card=new Card("1",1,false,0,0,true);
  cards:Card[]=[this.card];
  cardsAux:Card[]=[this.card];
  cardsValue:number[]=[0]
  cardsSort:number[]=[0];
  totalCard:number=15;
  level:number=3; //Se asignara el numero de cartas según nivel
  i:number=0;
  n:number=0;
  prefijo="../assets/images/Emprende_juego_";
  postfijo=".png";
  idSeleccionado:number=0;
  firtsSelect:number=10000;
  secondSelect:number=10000;
  aux:any;
  bandera:number= 0;
  firtsEnterVisit:boolean=false;
  count:number = 0;

  constructor(public modalCtrl: ModalController,  public navCtrl: NavController, public navParams: NavParams, /*private pblService: PblAppServiceProvider,*/
  private storage: Storage) {
  }
  ngAfterViewInit() {
    this.bandera = this.navParams.get('bandera');
    //let timer = TimerObservable.create(500, 1000);
    //  this.subscription = timer.subscribe(t => {
      if(this.bandera != 0){
            this.level = parseInt(this.navParams.get('nivel'));

      }
        this.generateArray();
        this.sorting();
        this.select();
        this.duplicate();
        this.sortingAll();
    //    this.subscription.unsubscribe();
    //  });

    if(this.bandera == 0){
      this.firtsEnter();
      console.log("Visita1:"+this.firtsEnterVisit);

    }
  }

  generateArray(){
    this.i=0;
    for(this.i=0; this.i<this.totalCard; this.i++){
      this.cardsValue[this.i]=this.i+1;
    }
  }

  sorting(){
    for (this.i = 0; this.i < this.cardsValue.length; this.i++) {
      const randomChoiceIndex = Math.floor(Math.random() * this.cardsValue.length);
      [this.cardsValue[this.i], this.cardsValue[randomChoiceIndex]] = [this.cardsValue[randomChoiceIndex], this.cardsValue[this.i]];
    }
  }
  select(){

     for (this.i = 0; this.i <this.level; this.i++) {
       this.card=new Card("1",1,false,0,0,false);
       this.card.url=this.prefijo+this.cardsValue[this.i]+this.postfijo;
       this.card.id=this.cardsValue[this.i];
       this.card.state=false;
       this.card.numCarta=1;
       this.card.index=this.i;
       this.card.readonly=false;
       this.cards[this.i]=this.card;
     }
  }

  duplicate(){
      this.cardsAux=this.cards;
      for(this.i=0; this.i< (this.level); this.i++){
        this.card=new Card("1",1,false,0,0,false);
        this.card.url=this.cards[this.i].url;
        this.card.id=this.cards[this.i].id;
        this.card.state=this.cards[this.i].state;
        this.card.numCarta=2;
        this.card.index=this.i+this.level;
        this.card.readonly=false;
        this.cards[this.i+this.level]=(this.card);
      }
    }

    sortingAll(){
      for (this.i = 0; this.i < this.cards.length; this.i++) {
        const randomChoiceIndex = Math.floor(Math.random() * this.cards.length);
        [this.cards[this.i], this.cards[randomChoiceIndex]] = [this.cards[randomChoiceIndex], this.cards[this.i]];
      }
    }
    flip(idCarta){
      if(this.cards[idCarta].state !=true){

        //this.cards[idCarta].readonly=false;
        if(this.firtsSelect != 10000 && this.secondSelect != 10000){
          console.log("no seleccionar");
        }else if(this.firtsSelect==10000){
            this.cards[idCarta].state=true;
          this.firtsSelect=idCarta;

        }else{
          this.cards[idCarta].state=true;
          this.secondSelect=idCarta;

          console.log("idCarta1: "+this.cards[this.firtsSelect].id);
          console.log("idCarta2: "+this.cards[this.secondSelect].id);
          if(this.cards[this.firtsSelect].id==this.cards[this.secondSelect].id
            && this.cards[this.firtsSelect].numCarta!=this.cards[this.secondSelect].numCarta
          ){

            //this.cards[this.firtsSelect].readonly=true;
            //this.cards[this.secondSelect].readonly=true;
            this.count++;

            console.log("count " + this.count);
            if(this.count == this.level){
                console.log("gano");
                this.save(this.level);
                this.presentModalWin();
                this.count = 0;
            }
            this.firtsSelect=10000;
            this.secondSelect=10000;

          }else{
                let timer = TimerObservable.create(400, 1000);
                  this.subscription = timer.subscribe(t => {
                    if(this.firtsSelect==10000 && this.secondSelect==10000){
                    }else{
                      this.cards[this.firtsSelect].state=false;
                      this.cards[this.secondSelect].state=false;
                      //this.cards[this.firtsSelect].readonly=true;
                      //this.cards[this.secondSelect].readonly=true;
                      this.firtsSelect=10000;
                      this.secondSelect=10000;
                    }

                    this.subscription.unsubscribe();
          });
  }
}
      }

  }
  presentModalMenu(){
    let menuModal = this.modalCtrl.create(ModalMenuPage);
    menuModal.present();
  }
  presentModalWin(){
    let winModal = this.modalCtrl.create(ModalWinEasyPage, {"level": this.level});
    winModal.present();
  }
  presentModalTuto(){
    let tutoModal = this.modalCtrl.create(ModalTutoPage);
    tutoModal.present();
  }
  save(level:number){
    let levelName;
    if(level ==3){
      levelName=1;
    }
    if(level ==9){
      levelName=2;
    }
    if(level==12){levelName=3;}
     //this.pblService.setEvent("juego_n"+levelName).subscribe();
  }
  firtsEnter(){
    this.storage.get('firtsEnter').then((val) => {
      if(val==null){
        this.storage.set('firtsEnter', '1');
        this.firtsEnterVisit=true;
          this.presentModalTuto();
      }else{
        this.firtsEnterVisit=false;
          this.presentModalMenu();
      }
    });
  }

  }
