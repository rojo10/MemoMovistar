/*import { Injectable } from '@angular/core';

import { NativeStorage } from '@ionic-native/native-storage';

import { MessagesAppServiceProvider } from '../../providers/messages-app-service/messages-app-service';
import { HttpRequestServiceProvider } from '../../providers/http-request-service/http-request-service';
import { UserprofileAppServiceProvider } from '../../providers/userprofile-app-service/userprofile-app-service';

class badgets{
  apr: boolean;
  fam: boolean;
  dis: boolean;
  esp: boolean;
  neg: boolean;
  noc: boolean;
  mad: boolean;
  emp: boolean;
  sab: boolean;
  mae: boolean;
  per: boolean;
  emr: boolean;
  constructor(apr, fam, dis, esp, neg, noc, mad, emp, sab, mae, per, emr){
    this.apr = apr;
    this.fam = fam;
    this.dis = dis;
    this.esp = esp;
    this.neg = neg;
    this.noc = noc;
    this.mad = mad;
    this.emp = emp;
    this.sab = sab;
    this.mae = mae;
    this.per = per;
    this.emr = emr;
  }
}

class medals{
  bro: boolean;
  pla: boolean;
  oro: boolean;
  constructor(bro, pla, oro){
    this.bro = bro;
    this.pla = pla;
    this.oro = oro;
  }
}

class points{
  ing: number;
  ins: number;
  med: number;
  sus: number;
  cha: number;
  fro: number;
  opi: number;
  tot: number;
  constructor(ing, ins, med, sus, cha, fro, opi){
    this.ing = ing;
    this.ins = ins;
    this.med = med;
    this.sus = sus;
    this.cha = cha;
    this.fro = fro;
    this.opi = opi;
    this.tot = ing + ins + med + sus + cha + fro + opi;
  }
}

@Injectable()
export class PblAppServiceProvider {

  private res: any;

  badget: badgets;
  medal: medals;
  point: points;

  constructor(
    private storage: NativeStorage,
    private httpReq: HttpRequestServiceProvider,
    public appmessages: MessagesAppServiceProvider,
    public userProfile: UserprofileAppServiceProvider,
  )
  {

  }

  private newProfile(){
    this.badget = new badgets(false, false, false, false, false, false, false, false, false, false, false, false);
    this.medal = new medals(false, false, false);
    this.point = new points(0, 0, 0, 0, 0, 0, 0);
  }

  private setProfile(pbl: any){
    this.badget = new badgets(pbl.apr, pbl.fam , pbl.dis , pbl.esp , pbl.neg , pbl.noc , pbl.mad , pbl.emp , pbl.sab, pbl.mae, pbl.per, pbl.emr);
    this.medal = new medals(pbl.bro, pbl.pla, pbl.oro);
    this.point = new points(pbl.ing, pbl.ins, pbl.med, pbl.sus, pbl.cha, pbl.fro, pbl.opi);
  }

  private joinProfile(badget: badgets, medal: medals, point: points){
    let pbl = {
      "apr": badget.apr,
      "fam": badget.fam,
      "dis": badget.dis,
      "esp": badget.esp,
      "neg": badget.neg,
      "noc": badget.noc,
      "mad": badget.mad,
      "emp": badget.emp,
      "sab": badget.sab,
      "mae": badget.mae,
      "per": badget.per,
      "emr": badget.emr,
      "bro": medal.bro,
      "pla": medal.pla,
      "oro": medal.oro,
      "ing": point.ing,
      "ins": point.ins,
      "med": point.med,
      "sus": point.sus,
      "cha": point.cha,
      "fro": point.fro,
      "opi": point.opi,
      "tot": point.tot,
    }
    return pbl;
  }

  public setEvent(evento: string){

    this.storage.getItem('_pblProfile').then(
      data => {
        this.res = (data) ? data : null;

        if(this.res)
        {
          console.log("Puntaje leido");
          this.setProfile(this.res);
        }
        else
        {
          console.log("Puntaje creado");
          this.newProfile();
        }

        switch(evento)
        {
          case 'ingreso':
            this.appmessages.showToaster('Ingresaste!');
          break;
          case 'perfil':
          break;
          case 'suscripcion':
          break;
          case 'video':
          break;
          case 'foro':
          break;
          case 'chat':
          break;
          case 'foro_coment':
          break;
          case 'chat_coment':
          break;
          case 'insignia':
          break;
          case 'medalla':
          break;
          case 'juego_n1':
          break;
          case 'juego_n2':
          break;
          case 'juego_n3':
          break;
        }

        this.storage.setItem('_pblProfile', this.joinProfile(this.badget, this.medal, this.point))
        .then(
          () => console.log('Puntaje guardado.'),
          error => console.error('Error guardando puntaje. ', error)
        );

      },
      error => {
        console.log("Error leyendo local", error);
        this.appmessages.showToaster('No podemos obtener tu puntaje...');
      }
    );
  }

}
*/
