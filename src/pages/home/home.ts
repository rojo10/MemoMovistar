import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {FlashCardComponent } from "../../components/flash-card/flash-card"


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {}

  ngAfterViewInit(){
    this.navCtrl.push(FlashCardComponent, {
      nivel: "3",
      bandera: 0,
    });
  }
}
