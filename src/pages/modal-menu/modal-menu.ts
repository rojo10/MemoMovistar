import { Component } from '@angular/core';
import { NavController,ToastController, ViewController,App} from 'ionic-angular';
import {FlashCardComponent } from "../../components/flash-card/flash-card"
/**
 * Generated class for the ModalMenuPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-modal-menu',
  templateUrl: 'modal-menu.html',
})
export class ModalMenuPage {

  value: string = "";

  constructor(public toastCtrl: ToastController, public viewCtrl: ViewController,public navCtrl:NavController,public appCtrl: App ) {}

  closeModal() {
    this.viewCtrl.dismiss();
  }

  mcqAnswer(questionID,answer){
	this.value = answer
  }

  logEvent() {
    this.closeModal();
    this.appCtrl.getRootNav().setRoot(FlashCardComponent, {
      nivel: this.value,
      bandera: 1,
    })
    .then(()=>{
    });
    //this.appCtrl.getRootNav().setRoot(FlashCardComponent);

  }

}
