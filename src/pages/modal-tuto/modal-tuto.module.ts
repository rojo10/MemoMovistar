import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalTutoPage } from './modal-tuto';

@NgModule({
  declarations: [
    ModalTutoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalTutoPage),
  ],
})
export class ModalTutoPageModule {}
