import { Component } from '@angular/core';
import { NavController,ViewController, NavParams ,App} from 'ionic-angular';
import {FlashCardComponent } from "../../components/flash-card/flash-card"

/**
 * Generated class for the ModalTutoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-modal-tuto',
  templateUrl: 'modal-tuto.html',
})
export class ModalTutoPage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams,public appCtrl: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalTutoPage');
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  logEvent(){
    this.closeModal();
    this.appCtrl.getRootNav().setRoot(FlashCardComponent, {
      nivel: 3,
      bandera: 0,
    })
    .then(()=>{
    });
  }

}
