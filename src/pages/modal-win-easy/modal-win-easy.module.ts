import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalWinEasyPage } from './modal-win-easy';

@NgModule({
  declarations: [
    ModalWinEasyPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalWinEasyPage),
  ],
})
export class ModalWinEasyPageModule {}
