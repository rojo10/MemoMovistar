import { Component } from '@angular/core';
import {  ViewController,NavController, NavParams,App } from 'ionic-angular';
import {FlashCardComponent } from "../../components/flash-card/flash-card"
/**
 * Generated class for the ModalWinEasyPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-modal-win-easy',
  templateUrl: 'modal-win-easy.html',
})
export class ModalWinEasyPage {
  public level:number=3;
  public path:string="assets/win_";
  public extension:string=".png";
  public url:string="";

  constructor(public navCtrl: NavController, public viewCtrl: ViewController,public navParams: NavParams,public appCtrl: App ) {
     this.level= this.navParams.get("level");
     this.url=this.path+this.level+this.extension;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalWinEasyPage');
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  logEvent(){
    this.closeModal();
    this.appCtrl.getRootNav().setRoot(FlashCardComponent, {
      nivel: 3,
      bandera: 0,
    })
    .then(()=>{
    });
  }


}
